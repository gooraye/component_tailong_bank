<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-12 17:42
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace byTest\component\tailong_bank\api;


use by\component\tailong_bank\api\ApproveDevApi;
use by\component\tailong_bank\api\ProxyBussDevelopQrcApi;
use by\component\tailong_bank\cache\FsCachePool;
use by\component\tailong_bank\context\BaseContext;
use by\component\tailong_bank\context\TestContext;
use by\component\tailong_bank\req\ApproveDevReq;
use by\component\tailong_bank\req\body\ProxyBussDevelopQrcReqBody;
use by\component\tailong_bank\req\ProxyBussDevelopQrcReq;
use by\component\tailong_bank\req\ReqHead;
use by\component\tailong_bank\resp\ApproveDevResp;
use by\infrastructure\base\CallResult;
use by\infrastructure\helper\CallResultHelper;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\InvalidArgumentException;

class ApproveDevApiTest extends TestCase
{
    /**
     * @throws InvalidArgumentException
     * @throws \ErrorException
     */
    public function testCall()
    {
        $context = new TestContext();
        $api = new ProxyBussDevelopQrcApi();
        $reqHead = new ReqHead();
        $seqNo = "201801031001";
        $reqHead->setMrchSno("ZF201803061525678980");
        $reqHead->setProductId($context->getProductId());
        $reqHead->setTxSno("jy201803061525678980");
        $reqHead->setTxTime("2018-03-16 16:12:12");
        $reqBody = new ProxyBussDevelopQrcReqBody();
        $reqBody->setOrderId("ZF20180306152567896");
        $reqBody->setRentCntrctNo("ZF201803061525678981");

        $reqBody->setAppID($context->getAppID());
        $reqBody->setClientName("张三");
        $reqBody->setGlobalId("330121196506146914");
        $reqBody->setMobile("15201955147");
        $reqBody->setHsNo("HZ0198770123");
        $reqBody->setHsProv("44");
        $reqBody->setHsArea("44");
        $reqBody->setHsCity("45");
        $reqBody->setHsStreetAdr("123");
        $reqBody->setHsMsg("555");
        $reqBody->setHouseAddress("77");
        $reqBody->setHouseArea("120");
        $reqBody->setRentHsMd("77");
        $reqBody->setChmg("2000");
        $reqBody->setPayPlgMd("77");
        $reqBody->setPymntMd("77");
        $reqBody->setFrstPayChmg("4000");
        $reqBody->setPlgAmt("2000");
        $reqBody->setRentStrtTm("20180306");
        $reqBody->setRentEndTm("20190305");
        $reqBody->setRentHsPps("77");
        $reqBody->setRentTrm("6");
        $reqBody->setLnldOrAgntNm("77");
        $reqBody->setLnldOrAgntGlobalTp("1");
        $reqBody->setLnldOrAgntGlobalId("330121198801111112");
        $reqBody->setLnldOrAgntMbl("134123456789");
        $reqBody->setHsItmdCoNm("777");
        $reqBody->setLnldFeePymntMd("777");
        $reqBody->setHsItmdPhnNo("13488888888");
        $reqBody->setPaymentMon("5");
        $reqBody->setLoanAmt("10000");

        $req = new ProxyBussDevelopQrcReq();
        $req->setReqBody($reqBody);
        $req->setReqHead($reqHead);
        $req->setSeqNo($seqNo);
        $result = $this->getAppAccessToken($context);
        if ($result->isFail()) {
            var_dump(mb_convert_encoding($result->getMsg(), "gbk", "utf-8"));
            return;
        }
        $appAccessToken = $result->getData();
//        echo 'appAccessToken = ' . $appAccessToken;
        var_dump($req->toArray());
        $resp = $api->call($appAccessToken, $req, $context);
        var_dump($resp);
        if ($resp->isFail()) {
            var_dump(mb_convert_encoding($resp->getMsg(), "gbk", "utf-8"));
        }
    }

    /**
     * @param BaseContext $context
     * @return CallResult
     * @throws \ErrorException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private function getAppAccessToken(BaseContext $context)
    {
        $api = new ApproveDevApi(new FsCachePool());
        $random = time() . rand(1000, 9999);
        $seqNo = date('YmdHis');
        $req = new ApproveDevReq();
        $req->setRandom($random);
        $req->setSeqNo($seqNo);
        $result = $api->call($req, $context);
        $data = $result->getData();
        if ($result->isSuccess() && $data instanceof ApproveDevResp) {
            return CallResultHelper::success($data->getAppAccessToken());
        } else {
            return CallResultHelper::fail($result->getMsg(), $result->getData());
        }
    }
}