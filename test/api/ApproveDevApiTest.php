<?php
///**
// * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
// * @author    hebidu<346551990@qq.com>
// * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
// * @link      http://www.itboye.com/
// * @license   http://www.opensource.org/licenses/mit-license.php MIT License
// * Revision History Version
// ********1.0.0********************
// * file created @ 2018-03-12 17:42
// *********************************
// ********1.0.1********************
// *
// *********************************
// */
//
//namespace byTest\component\tailong_bank\api;
//
//
//use by\component\tailong_bank\api\ApproveDevApi;
//use by\component\tailong_bank\context\TestContext;
//use by\component\tailong_bank\req\ApproveDevReq;
//use PHPUnit\Framework\TestCase;
//
//class ApproveDevApiTest extends TestCase
//{
//    /**
//     * @throws \ErrorException
//     */
//    public function testCall()
//    {
//        $api = new ApproveDevApi();
//        $random = time().rand(1000, 9999);
//        $seqNo = date('YmdHis');
//        $req = new ApproveDevReq();
//        $req->setRandom($random);
//        $req->setSeqNo($seqNo);
//        $resp = $api->call($req, new TestContext());
//        var_dump($resp);
//    }
//}