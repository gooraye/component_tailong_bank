<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-16 13:25
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace byTest\component\tailong_bank\aes;


use by\component\tailong_bank\helper\AesHelper;
use PHPUnit\Framework\TestCase;

class AesTest extends TestCase
{
    public function testIndex()
    {
        $content = "0f607264fc6318a92b9e13c65db7cd3c";
        $password = "mdzzmdzzmdzzmdzzmdzzmdzzmdzzmdzz";

        var_dump(AesHelper::opensslEncrypt($content, $password));

        var_dump(AesHelper::opensslDecrypt(("2PgLRjzlkpShwIHy1qtZnvLYQiAiDV/VzBd5x9oaEypyM/HEFIfFIcYUVrJkf56u"), $password));
    }

}