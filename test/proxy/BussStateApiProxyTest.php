<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-17 14:38
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace byTest\component\tailong_bank\proxy;


use by\component\tailong_bank\api_proxy\bussState\BussStateApiProxy;
use by\component\tailong_bank\api_proxy\bussState\BussStateParamsTdo;
use by\component\tailong_bank\context\TestContext;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class BussStateApiProxyTest extends TestCase
{
    /**
     * @throws \ErrorException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function testIndex()
    {
        $proxy = new BussStateApiProxy(new TestContext());
        $tdo = new BussStateParamsTdo();
        $seqNo = "201801031001";
        $tdo->setBussReqNo("2aaa984d9d9c4f4cb5b49fe3166d2b46");
        $tdo->setMrchSno("ZF201803061525678980");
        $tdo->setTxSno("jy201804061525678980");
        $tdo->setTxTime(date("Y-m-d H:i:s"));
        $tdo->setSeqNo($seqNo);
        $tdo->setOrderId("HO20180424141448192");
        $result = $proxy->call($tdo);
        if ($result->isFail()) {
//            var_dump($result->getMsg());
            var_dump(mb_convert_encoding($result->getMsg(), "gbk", "utf-8"));
            return;
        }
        Assert::assertFalse($result->isSuccess());
    }
}