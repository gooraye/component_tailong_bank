<?php
///**
// * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
// * @author    hebidu<346551990@qq.com>
// * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
// * @link      http://www.itboye.com/
// * @license   http://www.opensource.org/licenses/mit-license.php MIT License
// * Revision History Version
// ********1.0.0********************
// * file created @ 2018-03-17 11:43
// *********************************
// ********1.0.1********************
// *
// *********************************
// */
//
//namespace byTest\component\tailong_bank\proxy;
//
//
//use by\component\tailong_bank\api_proxy\qrcode\PBDApiProxy;
//use by\component\tailong_bank\api_proxy\qrcode\PBDParamsTdo;
//use by\component\tailong_bank\context\PreContext;
//use by\component\tailong_bank\context\ProductionContext;
//use by\component\tailong_bank\context\TestContext;
//use by\component\tailong_bank\resp\ProxyBussDevelopQrcResp;
//use PHPUnit\Framework\Assert;
//use PHPUnit\Framework\TestCase;
//
//class PBDApiProxyTest extends TestCase
//{
//    /**
//     * @throws \ErrorException
//     * @throws \Psr\SimpleCache\InvalidArgumentException
//     */
//    public function testIndex()
//    {
//        $proxy = new PBDApiProxy(new ProductionContext());
////        $proxy = new PBDApiProxy(new TestContext());
//        $tdo = new PBDParamsTdo();
//        $seqNo = "";
//        $tdo->setSeqNo($seqNo);
//        $tdo->setMrchSno("ZF2018041615256789807");
//        $tdo->setTxSno("jy20180416152567898065");
//        $tdo->setTxTime(date('Y-m-d H:i:s'));
//        $tdo->setOrderId("ZF2018041615256789626");
//        $tdo->setRentCntrctNo("ZF20180416152567899116");
//        $tdo->setClientName("何必都");
//        $tdo->setGlobalId("330327199108120957");
//        $tdo->setMobile("18557515452");
//        $tdo->setHsNo("HZ019877012664");
//        $tdo->setHsProv("浙江省");
//        $tdo->setHsArea("西湖区");
//        $tdo->setHsCity("杭州市");
//        $tdo->setHsStreetAdr("古屯路648号");
//        $tdo->setHsMsg("128平方米");
//        $tdo->setHouseAddress("77");
//        $tdo->setHouseArea("120");
//        $tdo->setRentHsMd("77");
//        $tdo->setChmg("2000");
//        $tdo->setPayPlgMd("77");
//        $tdo->setPymntMd("77");
//        $tdo->setFrstPayChmg("4000");
//        $tdo->setPlgAmt("2000");
//        $tdo->setRentStrtTm("20180306");
//        $tdo->setRentEndTm("20190305");
//        $tdo->setRentHsPps("77");
//        $tdo->setRentTrm("6");
//        $tdo->setLnldOrAgntNm("77");
//        $tdo->setLnldOrAgntGlobalTp("1");
//        $tdo->setLnldOrAgntGlobalId("330121198801111112");
//        $tdo->setLnldOrAgntMbl("134123456789");
//        $tdo->setHsItmdCoNm("777");
//        $tdo->setLnldFeePymntMd("777");
//        $tdo->setHsItmdPhnNo("13488888888");
//        $tdo->setPaymentMon("5");
//        $result = $proxy->call($tdo);
//        var_dump($result);
//        if ($result->isFail()) {
//            var_dump(mb_convert_encoding($result->getMsg(), "gbk", "utf-8"));
//            return;
//        }
//        $data = $result->getData();
//        if ($data instanceof ProxyBussDevelopQrcResp) {
//            var_dump($data->getRespHead()->toArray());
//            $arr = $data->getRespBody()->toArray();
//            unset($arr['qrCode']);
//            var_dump($arr);
//        }
//        Assert::assertFalse($result->isSuccess());
//    }
//}