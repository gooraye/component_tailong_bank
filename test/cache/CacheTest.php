<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-16 10:51
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace byTest\component\tailong_bank\cache;


use by\component\tailong_bank\cache\FsCachePool;
use by\component\tailong_bank\context\TestContext;
use PHPUnit\Framework\TestCase;

class CacheTest extends TestCase
{
    public function testIndex()
    {
        $context = new TestContext();
        $fsCachePool = new FsCachePool($context->getCachePath());

        $fsCachePool->set('test1', '120', 120);
        $fsCachePool->set('test2', 'yongjiu');
        $fsCachePool->set('test3', '3600', 3600);

        echo $fsCachePool->get('test1');
        echo $fsCachePool->get('test2');
        echo $fsCachePool->get('test3');
    }
}