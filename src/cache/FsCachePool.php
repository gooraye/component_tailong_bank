<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-16 10:44
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\cache;


use Cache\Adapter\Filesystem\FilesystemCachePool;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Psr\SimpleCache\CacheInterface;

class FsCachePool implements CacheInterface
{
    private $pool;

    public function __construct($path = __DIR__ . '/../../tmp/')
    {
        $filesystemAdapter = new Local($path);
        $filesystem = new Filesystem($filesystemAdapter);
        $this->pool = new FilesystemCachePool($filesystem);
    }

    public function get($key, $default = null)
    {
        return $this->pool->get($key, $default);
    }

    public function set($key, $value, $ttl = null)
    {
        return $this->pool->set($key, $value, $ttl);
    }

    public function delete($key)
    {
        return $this->pool->delete($key);
    }

    public function clear()
    {
        return $this->pool->clear();
    }

    public function getMultiple($keys, $default = null)
    {
        return $this->pool->getMultiple($keys, $default);
    }

    public function setMultiple($values, $ttl = null)
    {
        return $this->pool->setMultiple($values, $ttl);
    }

    public function deleteMultiple($keys)
    {
        return $this->pool->deleteMultiple($keys);
    }

    public function has($key)
    {
        return $this->pool->has($key);
    }


}