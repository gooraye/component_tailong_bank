<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-12 17:23
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\context;


class TestContext extends BaseContext
{
    public function __construct()
    {
        $this->setTokenCacheTime(1800);
        $this->setApiUrl("https://221.12.107.165/api/");
        $this->setAppID("6a052ffb_e2a8_4723_8b0d_b50911795964");
        $this->setAppSecretKey("cbc9c9ca-15da-4441-ac99-830dbc9df1b5");
        $this->setProductId('e665cebb7a7d4a17869dd051ad3896f2');
        $this->setCachePath(__DIR__ . '/../../../tmp/');
    }
}