<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-12 17:22
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\context;


class BaseContext
{
    private $appID;
    private $appSecretKey;
    private $apiUrl;
    private $productId;
    private $cachePath;
    private $tokenCacheTime;

    /**
     * @return mixed
     */
    public function getTokenCacheTime()
    {
        return $this->tokenCacheTime;
    }

    /**
     * @param mixed $tokenCacheTime
     */
    public function setTokenCacheTime($tokenCacheTime)
    {
        $this->tokenCacheTime = $tokenCacheTime;
    }

    /**
     * 缓存路径
     * @return mixed
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }

    /**
     * 缓存路径
     * @param string $cachePath
     */
    public function setCachePath($cachePath)
    {
        $this->cachePath = $cachePath;
    }

    /**
     * 产品ID - 固定
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * 请求地址
     * @return mixed
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * @param mixed $apiUrl
     */
    public function setApiUrl($apiUrl)
    {
        $this->apiUrl = $apiUrl;
    }

    /**
     * @return mixed
     */
    public function getAppID()
    {
        return $this->appID;
    }

    /**
     * @param mixed $appID
     */
    public function setAppID($appID)
    {
        $this->appID = $appID;
    }

    /**
     * @return mixed
     */
    public function getAppSecretKey()
    {
        return $this->appSecretKey;
    }

    /**
     * @param mixed $appSecretKey
     */
    public function setAppSecretKey($appSecretKey)
    {
        $this->appSecretKey = $appSecretKey;
    }
}