<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-17 14:33
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\api_proxy\notice;


use by\component\tailong_bank\api\SysNoticeApi;
use by\component\tailong_bank\api_proxy\BaseProxy;
use by\component\tailong_bank\context\BaseContext;
use by\component\tailong_bank\req\ReqHead;
use by\component\tailong_bank\req\SysNoticeReq;

class SysNoticeApiProxy extends BaseProxy
{

    public function __construct(BaseContext $context)
    {
        parent::__construct($context);
        $this->api = new SysNoticeApi();
        $this->context = $context;
    }

    /**
     * @param SysNoticeParamsTdo $paramsTdo
     * @return mixed
     * @throws \ErrorException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function call(SysNoticeParamsTdo $paramsTdo)
    {
        $accessTokenResult = $this->getAppAccessToken($this->context);
        if ($accessTokenResult->isFail()) {
            return $accessTokenResult;
        }
        $appAccessToken = $accessTokenResult->getData();
        $paramsTdo->setAppAccessToken($appAccessToken);

        $reqHead = new ReqHead();
        $seqNo = $paramsTdo->getSeqNo();
        $reqHead->setBussSeqNo($paramsTdo->getBussSeqNo());
        $reqHead->setMrchSno($paramsTdo->getMrchSno());
        $reqHead->setProductId($this->context->getProductId());
        $reqHead->setTxSno($paramsTdo->getTxSno());
        $reqHead->setTxTime($paramsTdo->getTxTime());

        $req = new SysNoticeReq();
        $req->setReqHead($reqHead);
        $req->setSeqNo($seqNo);
        $result = $this->api->call($paramsTdo->getAppAccessToken(), $req, $this->context);
        return $result;
    }
}