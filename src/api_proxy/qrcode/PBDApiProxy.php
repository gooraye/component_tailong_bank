<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-16 17:10
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\api_proxy\qrcode;


use by\component\tailong_bank\api\ProxyBussDevelopQrcApi;
use by\component\tailong_bank\api_proxy\BaseProxy;
use by\component\tailong_bank\context\BaseContext;
use by\component\tailong_bank\req\body\ProxyBussDevelopQrcReqBody;
use by\component\tailong_bank\req\ProxyBussDevelopQrcReq;
use by\component\tailong_bank\req\ReqHead;

/**
 * Class PBDApiProxy
 * @property ProxyBussDevelopQrcApi $api
 * @property BaseContext $context
 * @package by\component\tailong_bank\api_proxy\qrcode
 */
class PBDApiProxy extends BaseProxy
{

    public function __construct(BaseContext $context)
    {
        parent::__construct($context);
        $this->api = new ProxyBussDevelopQrcApi();
        $this->context = $context;
    }

    /**
     * @param PBDParamsTdo $paramsTdo
     * @return \by\infrastructure\base\CallResult
     * @throws \ErrorException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function call(PBDParamsTdo $paramsTdo)
    {
        $accessTokenResult = $this->getAppAccessToken($this->context);
        if ($accessTokenResult->isFail()) {
            return $accessTokenResult;
        }
        $appAccessToken = $accessTokenResult->getData();
        $paramsTdo->setAppAccessToken($appAccessToken);
        if (($chkResult = $paramsTdo->check()) && $chkResult->isFail()) {
            return $chkResult;
        }

        $reqHead = new ReqHead();
        $seqNo = $paramsTdo->getSeqNo();
        $reqHead->setMrchSno($paramsTdo->getMrchSno());
        $reqHead->setProductId($this->context->getProductId());
        $reqHead->setTxSno($paramsTdo->getTxSno());
        $reqHead->setTxTime($paramsTdo->getTxTime());
        $reqBody = new ProxyBussDevelopQrcReqBody();
        $reqBody->setOrderId($paramsTdo->getOrderId());
        $reqBody->setRentCntrctNo($paramsTdo->getRentCntrctNo());

        $reqBody->setAppID($this->context->getAppID());
        $reqBody->setClientName($paramsTdo->getClientName());
        $reqBody->setGlobalId($paramsTdo->getGlobalId());
        $reqBody->setMobile($paramsTdo->getMobile());
        $reqBody->setHsNo($paramsTdo->getHsNo());
        $reqBody->setHsProv($paramsTdo->getHsProv());
        $reqBody->setHsArea($paramsTdo->getHsArea());
        $reqBody->setHsCity($paramsTdo->getHsCity());
        $reqBody->setHsStreetAdr($paramsTdo->getHsStreetAdr());
        $reqBody->setHsMsg($paramsTdo->getHsMsg());
        $reqBody->setHouseAddress($paramsTdo->getHouseAddress());
        $reqBody->setHouseArea($paramsTdo->getHouseArea());
        $reqBody->setRentHsMd($paramsTdo->getRentHsMd());
        $reqBody->setChmg($paramsTdo->getChmg());
        $reqBody->setPayPlgMd($paramsTdo->getPayPlgMd());
        $reqBody->setPymntMd($paramsTdo->getPymntMd());
        $reqBody->setFrstPayChmg($paramsTdo->getFrstPayChmg());
        $reqBody->setPlgAmt($paramsTdo->getPlgAmt());
        $reqBody->setRentStrtTm($paramsTdo->getRentStrtTm());
        $reqBody->setRentEndTm($paramsTdo->getRentEndTm());
        $reqBody->setRentHsPps($paramsTdo->getRentHsPps());
        $reqBody->setRentTrm($paramsTdo->getRentTrm());
        $reqBody->setLnldOrAgntNm($paramsTdo->getLnldOrAgntNm());
        $reqBody->setLnldOrAgntGlobalTp($paramsTdo->getLnldOrAgntGlobalTp());
        $reqBody->setLnldOrAgntGlobalId($paramsTdo->getLnldOrAgntGlobalId());
        $reqBody->setLnldOrAgntMbl($paramsTdo->getLnldOrAgntMbl());
        $reqBody->setHsItmdCoNm($paramsTdo->getHsItmdCoNm());
        $reqBody->setLnldFeePymntMd($paramsTdo->getLnldFeePymntMd());
        $reqBody->setHsItmdPhnNo($paramsTdo->getHsItmdPhnNo());
        $reqBody->setPaymentMon($paramsTdo->getPaymentMon());
        $reqBody->setLoanAmt(strval(intval($paramsTdo->getPaymentMon() * $paramsTdo->getChmg())));

        $req = new ProxyBussDevelopQrcReq();
        $req->setReqBody($reqBody);
        $req->setReqHead($reqHead);
        $req->setSeqNo($seqNo);

        $result = $this->api->call($paramsTdo->getAppAccessToken(), $req, $this->context);
        return $result;
    }
}