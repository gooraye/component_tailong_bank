<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-17 10:02
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\api_proxy\qrcode;


use by\infrastructure\helper\CallResultHelper;
use by\infrastructure\interfaces\CheckInterfaces;

class PBDParamsTdo implements CheckInterfaces
{
    private $appAccessToken;
    private $seqNo;
    private $mrchSno;
    private $txSno;
    private $txTime;
    private $clientName;
    private $globalId;
    private $mobile;
    private $orderId;
    private $hsNo;
    private $hsProv;
    private $hsCity;
    private $hsArea;
    private $hsStreetAdr;
    private $hsMsg;
    private $houseAddress;
    private $houseArea;
    private $rentHsMd;
    private $chmg;
    private $payPlgMd;
    private $pymntMd;
    private $frstPayChmg;
    private $plgAmt;
    private $rentStrtTm;
    private $rentEndTm;
    private $rentHsPps;
    private $rentTrm;
    private $lnldOrAgntNm;
    private $lnldOrAgntGlobalTp;
    private $lnldOrAgntGlobalId;
    private $lnldOrAgntMbl;
    private $hsItmdCoNm;
    private $lnldFeePymntMd;
    private $hsItmdPhnNo;
    private $rentCntrctNo;
    private $loanAmt;
    private $paymentMon;

    function check()
    {
        $checkProperty = ["appAccessToken", "clientName"];
        foreach ($checkProperty as $value) {
            $method = "get" . ucfirst($value);
            if (method_exists($this, $method)) {
                if (empty($this->$method())) {
                    return CallResultHelper::fail("[ZhuJia]" . $value . "缺失");
                }

            }
        }
        return CallResultHelper::success();
    }

    /**
     * @return mixed
     */
    public function getTxTime()
    {
        return $this->txTime;
    }

    /**
     * @param mixed $txTime
     */
    public function setTxTime($txTime)
    {
        $this->txTime = $txTime;
    }

    /**
     * @return mixed
     */
    public function getTxSno()
    {
        return $this->txSno;
    }

    /**
     * @param mixed $txSno
     */
    public function setTxSno($txSno)
    {
        $this->txSno = $txSno;
    }

    /**
     * @return mixed
     */
    public function getMrchSno()
    {
        return $this->mrchSno;
    }

    /**
     * @param mixed $mrchSno
     */
    public function setMrchSno($mrchSno)
    {
        $this->mrchSno = $mrchSno;
    }

    /**
     * @return mixed
     */
    public function getSeqNo()
    {
        return $this->seqNo;
    }

    /**
     * @param mixed $seqNo
     */
    public function setSeqNo($seqNo)
    {
        $this->seqNo = $seqNo;
    }

    /**
     * @return mixed
     */
    public function getAppAccessToken()
    {
        return $this->appAccessToken;
    }

    /**
     * @param mixed $appAccessToken
     */
    public function setAppAccessToken($appAccessToken)
    {
        $this->appAccessToken = $appAccessToken;
    }

    /**
     * @return mixed
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @param mixed $clientName
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;
    }

    /**
     * @return mixed
     */
    public function getGlobalId()
    {
        return $this->globalId;
    }

    /**
     * @param mixed $globalId
     */
    public function setGlobalId($globalId)
    {
        $this->globalId = $globalId;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param mixed $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getHsNo()
    {
        return $this->hsNo;
    }

    /**
     * @param mixed $hsNo
     */
    public function setHsNo($hsNo)
    {
        $this->hsNo = $hsNo;
    }

    /**
     * @return mixed
     */
    public function getHsProv()
    {
        return $this->hsProv;
    }

    /**
     * @param mixed $hsProv
     */
    public function setHsProv($hsProv)
    {
        $this->hsProv = $hsProv;
    }

    /**
     * @return mixed
     */
    public function getHsCity()
    {
        return $this->hsCity;
    }

    /**
     * @param mixed $hsCity
     */
    public function setHsCity($hsCity)
    {
        $this->hsCity = $hsCity;
    }

    /**
     * @return mixed
     */
    public function getHsArea()
    {
        return $this->hsArea;
    }

    /**
     * @param mixed $hsArea
     */
    public function setHsArea($hsArea)
    {
        $this->hsArea = $hsArea;
    }

    /**
     * @return mixed
     */
    public function getHsStreetAdr()
    {
        return $this->hsStreetAdr;
    }

    /**
     * @param mixed $hsStreetAdr
     */
    public function setHsStreetAdr($hsStreetAdr)
    {
        $this->hsStreetAdr = $hsStreetAdr;
    }

    /**
     * @return mixed
     */
    public function getHsMsg()
    {
        return $this->hsMsg;
    }

    /**
     * @param mixed $hsMsg
     */
    public function setHsMsg($hsMsg)
    {
        $this->hsMsg = $hsMsg;
    }

    /**
     * @return mixed
     */
    public function getHouseAddress()
    {
        return $this->houseAddress;
    }

    /**
     * @param mixed $houseAddress
     */
    public function setHouseAddress($houseAddress)
    {
        $this->houseAddress = $houseAddress;
    }

    /**
     * @return mixed
     */
    public function getHouseArea()
    {
        return $this->houseArea;
    }

    /**
     * @param mixed $houseArea
     */
    public function setHouseArea($houseArea)
    {
        $this->houseArea = $houseArea;
    }

    /**
     * @return mixed
     */
    public function getRentHsMd()
    {
        return $this->rentHsMd;
    }

    /**
     * @param mixed $rentHsMd
     */
    public function setRentHsMd($rentHsMd)
    {
        $this->rentHsMd = $rentHsMd;
    }

    /**
     * @return mixed
     */
    public function getChmg()
    {
        return $this->chmg;
    }

    /**
     * @param mixed $chmg
     */
    public function setChmg($chmg)
    {
        $this->chmg = $chmg;
    }

    /**
     * @return mixed
     */
    public function getPayPlgMd()
    {
        return $this->payPlgMd;
    }

    /**
     * @param mixed $payPlgMd
     */
    public function setPayPlgMd($payPlgMd)
    {
        $this->payPlgMd = $payPlgMd;
    }

    /**
     * @return mixed
     */
    public function getPymntMd()
    {
        return $this->pymntMd;
    }

    /**
     * @param mixed $pymntMd
     */
    public function setPymntMd($pymntMd)
    {
        $this->pymntMd = $pymntMd;
    }

    /**
     * @return mixed
     */
    public function getFrstPayChmg()
    {
        return $this->frstPayChmg;
    }

    /**
     * @param mixed $frstPayChmg
     */
    public function setFrstPayChmg($frstPayChmg)
    {
        $this->frstPayChmg = $frstPayChmg;
    }

    /**
     * @return mixed
     */
    public function getPlgAmt()
    {
        return $this->plgAmt;
    }

    /**
     * @param mixed $plgAmt
     */
    public function setPlgAmt($plgAmt)
    {
        $this->plgAmt = $plgAmt;
    }

    /**
     * @return mixed
     */
    public function getRentStrtTm()
    {
        return $this->rentStrtTm;
    }

    /**
     * @param mixed $rentStrtTm
     */
    public function setRentStrtTm($rentStrtTm)
    {
        $this->rentStrtTm = $rentStrtTm;
    }

    /**
     * @return mixed
     */
    public function getRentEndTm()
    {
        return $this->rentEndTm;
    }

    /**
     * @param mixed $rentEndTm
     */
    public function setRentEndTm($rentEndTm)
    {
        $this->rentEndTm = $rentEndTm;
    }

    /**
     * @return mixed
     */
    public function getRentHsPps()
    {
        return $this->rentHsPps;
    }

    /**
     * @param mixed $rentHsPps
     */
    public function setRentHsPps($rentHsPps)
    {
        $this->rentHsPps = $rentHsPps;
    }

    /**
     * @return mixed
     */
    public function getRentTrm()
    {
        return $this->rentTrm;
    }

    /**
     * @param mixed $rentTrm
     */
    public function setRentTrm($rentTrm)
    {
        $this->rentTrm = $rentTrm;
    }

    /**
     * @return mixed
     */
    public function getLnldOrAgntNm()
    {
        return $this->lnldOrAgntNm;
    }

    /**
     * @param mixed $lnldOrAgntNm
     */
    public function setLnldOrAgntNm($lnldOrAgntNm)
    {
        $this->lnldOrAgntNm = $lnldOrAgntNm;
    }

    /**
     * @return mixed
     */
    public function getLnldOrAgntGlobalTp()
    {
        return $this->lnldOrAgntGlobalTp;
    }

    /**
     * @param mixed $lnldOrAgntGlobalTp
     */
    public function setLnldOrAgntGlobalTp($lnldOrAgntGlobalTp)
    {
        $this->lnldOrAgntGlobalTp = $lnldOrAgntGlobalTp;
    }

    /**
     * @return mixed
     */
    public function getLnldOrAgntGlobalId()
    {
        return $this->lnldOrAgntGlobalId;
    }

    /**
     * @param mixed $lnldOrAgntGlobalId
     */
    public function setLnldOrAgntGlobalId($lnldOrAgntGlobalId)
    {
        $this->lnldOrAgntGlobalId = $lnldOrAgntGlobalId;
    }

    /**
     * @return mixed
     */
    public function getLnldOrAgntMbl()
    {
        return $this->lnldOrAgntMbl;
    }

    /**
     * @param mixed $lnldOrAgntMbl
     */
    public function setLnldOrAgntMbl($lnldOrAgntMbl)
    {
        $this->lnldOrAgntMbl = $lnldOrAgntMbl;
    }

    /**
     * @return mixed
     */
    public function getHsItmdCoNm()
    {
        return $this->hsItmdCoNm;
    }

    /**
     * @param mixed $hsItmdCoNm
     */
    public function setHsItmdCoNm($hsItmdCoNm)
    {
        $this->hsItmdCoNm = $hsItmdCoNm;
    }

    /**
     * @return mixed
     */
    public function getLnldFeePymntMd()
    {
        return $this->lnldFeePymntMd;
    }

    /**
     * @param mixed $lnldFeePymntMd
     */
    public function setLnldFeePymntMd($lnldFeePymntMd)
    {
        $this->lnldFeePymntMd = $lnldFeePymntMd;
    }

    /**
     * @return mixed
     */
    public function getHsItmdPhnNo()
    {
        return $this->hsItmdPhnNo;
    }

    /**
     * @param mixed $hsItmdPhnNo
     */
    public function setHsItmdPhnNo($hsItmdPhnNo)
    {
        $this->hsItmdPhnNo = $hsItmdPhnNo;
    }

    /**
     * @return mixed
     */
    public function getRentCntrctNo()
    {
        return $this->rentCntrctNo;
    }

    /**
     * @param mixed $rentCntrctNo
     */
    public function setRentCntrctNo($rentCntrctNo)
    {
        $this->rentCntrctNo = $rentCntrctNo;
    }

    /**
     * @return mixed
     */
    public function getLoanAmt()
    {
        return $this->loanAmt;
    }

    /**
     * @param mixed $loanAmt
     */
    public function setLoanAmt($loanAmt)
    {
        $this->loanAmt = $loanAmt;
    }

    /**
     * @return mixed
     */
    public function getPaymentMon()
    {
        return $this->paymentMon;
    }

    /**
     * @param mixed $paymentMon
     */
    public function setPaymentMon($paymentMon)
    {
        $this->paymentMon = $paymentMon;
    }
}