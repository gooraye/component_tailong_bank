<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-17 15:43
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\api_proxy\bussState;


use by\component\tailong_bank\api_proxy\BaseParamsDto;

class BatchBussStateParamsTdo extends BaseParamsDto
{
    private $strtTime;
    private $endTime;

    /**
     * @return mixed
     */
    public function getStrtTime()
    {
        return $this->strtTime;
    }

    /**
     * @param mixed $strtTime
     */
    public function setStrtTime($strtTime)
    {
        $this->strtTime = $strtTime;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param mixed $endTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }
}