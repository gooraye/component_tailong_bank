<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-15 14:01
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\helper;


class AesHelper
{
    /**
     * [opensslDecrypt description]
     * 使用openssl库进行加密
     * @param $sStr
     * @param $sKey
     * @param string $method
     * @return string [type]
     */
    public static function opensslEncrypt($sStr, $sKey, $method = 'aes-256-cbc')
    {
        $iv = "abcdefghABCDEFGH";
        $str = openssl_encrypt($sStr, $method, $sKey, 0, $iv);
        return ($str);
    }

    /**
     * [opensslDecrypt description]
     * 使用openssl库进行解密
     * @param $sStr
     * @param $sKey
     * @param string $method
     * @return string [type]
     */
    public static function opensslDecrypt($sStr, $sKey, $method = 'aes-256-cbc')
    {
        $iv = "abcdefghABCDEFGH";
        $str = openssl_decrypt($sStr, $method, $sKey, 0, $iv);
        return $str;
    }
}