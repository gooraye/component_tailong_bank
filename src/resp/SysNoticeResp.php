<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-17 14:12
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\resp;

/**
 * Class SysNoticeResp
 * @property
 * @package by\component\tailong_bank\resp
 */
class SysNoticeResp extends BaseResp
{


    private $sysNoticeCode;
    private $sysNoticeCntnt;

    /**
     * SysNoticeResp constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->respHead = new RespHead();
        parent::__construct($data);
    }

    /**
     * @return mixed
     */
    public function getSysNoticeCode()
    {
        return $this->sysNoticeCode;
    }

    /**
     * @param mixed $sysNoticeCode
     */
    public function setSysNoticeCode($sysNoticeCode)
    {
        $this->sysNoticeCode = $sysNoticeCode;
    }

    /**
     * @return mixed
     */
    public function getSysNoticeCntnt()
    {
        return $this->sysNoticeCntnt;
    }

    /**
     * @param mixed $sysNoticeCntnt
     */
    public function setSysNoticeCntnt($sysNoticeCntnt)
    {
        $this->sysNoticeCntnt = $sysNoticeCntnt;
    }


}