<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-16 16:06
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\resp;


use by\component\tailong_bank\helper\Obj2ArrayExtendHelper;
use by\infrastructure\interfaces\ObjectToArrayInterface;

class RespHead implements ObjectToArrayInterface
{
    private $txSno;
    private $errorCode;
    private $errorMsg;

    /**
     * @return mixed
     */
    public function getTxSno()
    {
        return $this->txSno;
    }

    /**
     * @param mixed $txSno
     */
    public function setTxSno($txSno)
    {
        $this->txSno = $txSno;
    }


    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    /**
     * @param mixed $errorMsg
     */
    public function setErrorMsg($errorMsg)
    {
        $this->errorMsg = $errorMsg;
    }

    public function toArray()
    {
        return Obj2ArrayExtendHelper::getArrayFrom($this);
    }
}