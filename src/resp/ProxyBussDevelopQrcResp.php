<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-13 09:58
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\resp;


use by\component\tailong_bank\resp\body\ProxyBussDevelopQrcRespBody;

/**
 * Class ProxyBussDevelopQrcResp
 * @property ProxyBussDevelopQrcRespBody $respBody
 * @package by\component\tailong_bank\resp
 */
class ProxyBussDevelopQrcResp extends BaseResp
{

    /**
     * ProxyBussDevelopQrcResp constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->respHead = new RespHead();
        $this->respBody = new ProxyBussDevelopQrcRespBody();
        parent::__construct($data);
    }

    public function toArray()
    {
        return [
            'body' => $this->respBody->toArray(),
            'head' => $this->respHead->toArray()
        ];
    }

    /**
     * @return RespHead
     */
    public function getRespHead()
    {
        return $this->respHead;
    }

    /**
     * @param RespHead $respHead
     */
    public function setRespHead($respHead)
    {
        $this->respHead = $respHead;
    }

    /**
     * @return ProxyBussDevelopQrcRespBody
     */
    public function getRespBody()
    {
        return $this->respBody;
    }

    /**
     * @param ProxyBussDevelopQrcRespBody $respBody
     */
    public function setRespBody($respBody)
    {
        $this->respBody = $respBody;
    }

}