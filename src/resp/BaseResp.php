<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-16 10:57
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\resp;


use by\infrastructure\helper\Object2DataArrayHelper;
use by\infrastructure\interfaces\ObjectToArrayInterface;

/**
 * Class BaseResp
 * @property RespHead $respHead
 * @package by\component\tailong_bank\resp
 */
class BaseResp implements ObjectToArrayInterface
{


    protected $errorCode;
    protected $errorMsg;
    protected $appID;
    protected $seqNO;
    protected $respHead;
    protected $respBody;
    protected $_rspData;

    /**
     * BaseResp constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        if (array_key_exists('_rspData', $data)) {
            $rspData = $data['_rspData'];
            if (array_key_exists('head', $rspData)) {
                Object2DataArrayHelper::setData($this->respHead, $rspData['head']);
            }
            if (array_key_exists('body', $rspData)) {
                Object2DataArrayHelper::setData($this, $rspData['body']);
                Object2DataArrayHelper::setData($this->respBody, $rspData['body']);
            }
        }
        $this->_rspData = $data['_rspData'];
        unset($data['_rspData']);
        Object2DataArrayHelper::setData($this, $data);
    }


    public function isFail()
    {
        return !$this->isSuccess();
    }

    public function isSuccess()
    {
        if (!empty($this->getErrorCode()) && $this->getErrorCode() != '000000') {
            return false;
        }
        $isSuccess = $this->respHead->getErrorCode() == "PAAS-E-OAuth-000000";

        return ($this->respHead->getErrorCode() == '000000' || $isSuccess);
    }

    /**
     * @return mixed
     */
    public function getRspData()
    {
        return $this->_rspData;
    }

    /**
     * @param mixed $rspData
     */
    public function setRspData($rspData)
    {
        $this->_rspData = $rspData;
    }



    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMsg()
    {
        $msg = $this->errorMsg;
        if (!empty($this->respHead) && !empty($this->respHead->getErrorMsg())) {
            $msg .= '[sub]' . $this->respHead->getErrorMsg();
        }
        return $msg;
    }

    /**
     * @param mixed $errorMsg
     */
    public function setErrorMsg($errorMsg)
    {
        $this->errorMsg = $errorMsg;
    }

    /**
     * @return mixed
     */
    public function getAppID()
    {
        return $this->appID;
    }

    /**
     * @param mixed $appID
     */
    public function setAppID($appID)
    {
        $this->appID = $appID;
    }

    /**
     * @return mixed
     */
    public function getSeqNO()
    {
        return $this->seqNO;
    }

    /**
     * @param mixed $seqNO
     */
    public function setSeqNO($seqNO)
    {
        $this->seqNO = $seqNO;
    }

    /**
     * @return mixed
     */
    public function getRespHead()
    {
        return $this->respHead;
    }

    /**
     * @param mixed $respHead
     */
    public function setRespHead($respHead)
    {
        $this->respHead = $respHead;
    }

    /**
     * @return mixed
     */
    public function getRespBody()
    {
        return $this->respBody;
    }

    /**
     * @param mixed $respBody
     */
    public function setRespBody($respBody)
    {
        $this->respBody = $respBody;
    }


    public function toArray()
    {
        return Object2DataArrayHelper::getDataArrayFrom($this);
    }
}