<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-17 15:37
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\resp\body;


class BussStateRespBody
{
    private $bussSeqNo;
    private $orderId;
    private $rentCntrctNo;
    private $txTime;
    private $bussStateCode;
    private $bussStateMdfMsg;
    private $loanAmt;
    private $clientName;
    private $notRpyAmt;
    private $rpyTime;
    private $crnOdueState;
    private $rpyDt;
    private $crnOdueDays;
    private $crnOdueAmt;
    private $argnAmt;
    private $argnAmtAdd;
    private $dailyLimTranAmt;
    private $dailyLimTranBal;

    /**
     * @return mixed
     */
    public function getBussSeqNo()
    {
        return $this->bussSeqNo;
    }

    /**
     * @param mixed $bussSeqNo
     */
    public function setBussSeqNo($bussSeqNo)
    {
        $this->bussSeqNo = $bussSeqNo;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getRentCntrctNo()
    {
        return $this->rentCntrctNo;
    }

    /**
     * @param mixed $rentCntrctNo
     */
    public function setRentCntrctNo($rentCntrctNo)
    {
        $this->rentCntrctNo = $rentCntrctNo;
    }

    /**
     * @return mixed
     */
    public function getTxTime()
    {
        return $this->txTime;
    }

    /**
     * @param mixed $txTime
     */
    public function setTxTime($txTime)
    {
        $this->txTime = $txTime;
    }

    /**
     * @return mixed
     */
    public function getBussStateCode()
    {
        return $this->bussStateCode;
    }

    /**
     * @param mixed $bussStateCode
     */
    public function setBussStateCode($bussStateCode)
    {
        $this->bussStateCode = $bussStateCode;
    }

    /**
     * @return mixed
     */
    public function getBussStateMdfMsg()
    {
        return $this->bussStateMdfMsg;
    }

    /**
     * @param mixed $bussStateMdfMsg
     */
    public function setBussStateMdfMsg($bussStateMdfMsg)
    {
        $this->bussStateMdfMsg = $bussStateMdfMsg;
    }

    /**
     * @return mixed
     */
    public function getLoanAmt()
    {
        return $this->loanAmt;
    }

    /**
     * @param mixed $loanAmt
     */
    public function setLoanAmt($loanAmt)
    {
        $this->loanAmt = $loanAmt;
    }

    /**
     * @return mixed
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @param mixed $clientName
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;
    }

    /**
     * @return mixed
     */
    public function getNotRpyAmt()
    {
        return $this->notRpyAmt;
    }

    /**
     * @param mixed $notRpyAmt
     */
    public function setNotRpyAmt($notRpyAmt)
    {
        $this->notRpyAmt = $notRpyAmt;
    }

    /**
     * @return mixed
     */
    public function getRpyTime()
    {
        return $this->rpyTime;
    }

    /**
     * @param mixed $rpyTime
     */
    public function setRpyTime($rpyTime)
    {
        $this->rpyTime = $rpyTime;
    }

    /**
     * @return mixed
     */
    public function getCrnOdueState()
    {
        return $this->crnOdueState;
    }

    /**
     * @param mixed $crnOdueState
     */
    public function setCrnOdueState($crnOdueState)
    {
        $this->crnOdueState = $crnOdueState;
    }

    /**
     * @return mixed
     */
    public function getRpyDt()
    {
        return $this->rpyDt;
    }

    /**
     * @param mixed $rpyDt
     */
    public function setRpyDt($rpyDt)
    {
        $this->rpyDt = $rpyDt;
    }

    /**
     * @return mixed
     */
    public function getCrnOdueDays()
    {
        return $this->crnOdueDays;
    }

    /**
     * @param mixed $crnOdueDays
     */
    public function setCrnOdueDays($crnOdueDays)
    {
        $this->crnOdueDays = $crnOdueDays;
    }

    /**
     * @return mixed
     */
    public function getCrnOdueAmt()
    {
        return $this->crnOdueAmt;
    }

    /**
     * @param mixed $crnOdueAmt
     */
    public function setCrnOdueAmt($crnOdueAmt)
    {
        $this->crnOdueAmt = $crnOdueAmt;
    }

    /**
     * @return mixed
     */
    public function getArgnAmt()
    {
        return $this->argnAmt;
    }

    /**
     * @param mixed $argnAmt
     */
    public function setArgnAmt($argnAmt)
    {
        $this->argnAmt = $argnAmt;
    }

    /**
     * @return mixed
     */
    public function getArgnAmtAdd()
    {
        return $this->argnAmtAdd;
    }

    /**
     * @param mixed $argnAmtAdd
     */
    public function setArgnAmtAdd($argnAmtAdd)
    {
        $this->argnAmtAdd = $argnAmtAdd;
    }

    /**
     * @return mixed
     */
    public function getDailyLimTranAmt()
    {
        return $this->dailyLimTranAmt;
    }

    /**
     * @param mixed $dailyLimTranAmt
     */
    public function setDailyLimTranAmt($dailyLimTranAmt)
    {
        $this->dailyLimTranAmt = $dailyLimTranAmt;
    }

    /**
     * @return mixed
     */
    public function getDailyLimTranBal()
    {
        return $this->dailyLimTranBal;
    }

    /**
     * @param mixed $dailyLimTranBal
     */
    public function setDailyLimTranBal($dailyLimTranBal)
    {
        $this->dailyLimTranBal = $dailyLimTranBal;
    }
}