<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-16 16:07
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\resp\body;


use by\component\tailong_bank\helper\Obj2ArrayExtendHelper;
use by\infrastructure\interfaces\ObjectToArrayInterface;

class ProxyBussDevelopQrcRespBody implements ObjectToArrayInterface
{
    private $qrcUrl;
    private $qrCode;
    private $bussSeqNo;

    public function toArray()
    {
        return Obj2ArrayExtendHelper::getArrayFrom($this);
    }

    /**
     * @return mixed
     */
    public function getQrcUrl()
    {
        return $this->qrcUrl;
    }

    /**
     * @param mixed $qrcUrl
     */
    public function setQrcUrl($qrcUrl)
    {
        $this->qrcUrl = $qrcUrl;
    }

    /**
     * @return mixed
     */
    public function getQrCode()
    {
        return $this->qrCode;
    }

    /**
     * @param mixed $qrCode
     */
    public function setQrCode($qrCode)
    {
        $this->qrCode = $qrCode;
    }

    /**
     * @return mixed
     */
    public function getBussSeqNo()
    {
        return $this->bussSeqNo;
    }

    /**
     * @param mixed $bussSeqNo
     */
    public function setBussSeqNo($bussSeqNo)
    {
        $this->bussSeqNo = $bussSeqNo;
    }
}