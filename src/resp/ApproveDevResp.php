<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-16 10:54
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\resp;


use by\infrastructure\helper\Object2DataArrayHelper;

class ApproveDevResp extends BaseResp
{
    private $appAccessToken;
    private $appParam;

    /**
     * ApproveDevResp constructor.
     * @param $data
     */
    public function __construct($data = [])
    {
        if (is_string($data)) {
            $data = json_decode($data, JSON_OBJECT_AS_ARRAY);
        }
        if (is_array($data)) {
            Object2DataArrayHelper::setData($this, $data);
        }
    }


    /**
     * @return mixed
     */
    public function getAppAccessToken()
    {
        return $this->appAccessToken;
    }

    /**
     * @param mixed $appAccessToken
     */
    public function setAppAccessToken($appAccessToken)
    {
        $this->appAccessToken = $appAccessToken;
    }

    /**
     * @return mixed
     */
    public function getAppParam()
    {
        return $this->appParam;
    }

    /**
     * @param mixed $appParam
     */
    public function setAppParam($appParam)
    {
        $this->appParam = $appParam;
    }


}