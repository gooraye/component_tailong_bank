<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-12 17:18
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\req;


use by\component\tailong_bank\helper\Obj2ArrayExtendHelper;
use by\infrastructure\interfaces\ObjectToArrayInterface;

class ReqHead implements ObjectToArrayInterface
{
    private $txSno;
    private $mrchSno;
    private $bussSeqNo;
    private $txTime;
    private $productId;

    /**
     * @return mixed
     */
    public function getTxSno()
    {
        return $this->txSno;
    }

    /**
     * @param mixed $txSno
     */
    public function setTxSno($txSno)
    {
        $this->txSno = $txSno;
    }

    /**
     * @return mixed
     */
    public function getMrchSno()
    {
        return $this->mrchSno;
    }

    /**
     * @param mixed $mrchSno
     */
    public function setMrchSno($mrchSno)
    {
        $this->mrchSno = $mrchSno;
    }

    /**
     * @return mixed
     */
    public function getBussSeqNo()
    {
        return $this->bussSeqNo;
    }

    /**
     * @param mixed $bussSeqNo
     */
    public function setBussSeqNo($bussSeqNo)
    {
        $this->bussSeqNo = $bussSeqNo;
    }

    /**
     * @return mixed
     */
    public function getTxTime()
    {
        return $this->txTime;
    }

    /**
     * @param mixed $txTime
     */
    public function setTxTime($txTime)
    {
        $this->txTime = $txTime;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    public function toArray()
    {
        return Obj2ArrayExtendHelper::getArrayFrom($this);
    }
}