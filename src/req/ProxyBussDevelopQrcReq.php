<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-13 09:58
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\req;


use by\component\tailong_bank\req\body\ProxyBussDevelopQrcReqBody;
use by\infrastructure\interfaces\ObjectToArrayInterface;

class ProxyBussDevelopQrcReq implements ObjectToArrayInterface
{
    public function toArray()
    {
        return [
            'body' => $this->reqBody->toArray(),
            'head' => $this->reqHead->toArray()
        ];
    }

    private $seqNo;

    /**
     * @return mixed
     */
    public function getSeqNo()
    {
        return $this->seqNo;
    }

    /**
     * @param mixed $seqNo
     */
    public function setSeqNo($seqNo)
    {
        $this->seqNo = $seqNo;
    }

    /**
     * @var ReqHead
     */
    private $reqHead;

    /**
     * @var ProxyBussDevelopQrcReqBody
     */
    private $reqBody;

    // construct
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getReqHead()
    {
        return $this->reqHead;
    }

    /**
     * @param mixed $reqHead
     */
    public function setReqHead($reqHead)
    {
        $this->reqHead = $reqHead;
    }

    /**
     * @return mixed
     */
    public function getReqBody()
    {
        return $this->reqBody;
    }

    /**
     * @param mixed $reqBody
     */
    public function setReqBody($reqBody)
    {
        $this->reqBody = $reqBody;
    }
}