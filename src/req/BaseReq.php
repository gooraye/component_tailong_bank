<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-17 14:08
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\req;


use by\component\tailong_bank\helper\Obj2ArrayExtendHelper;
use by\infrastructure\interfaces\ObjectToArrayInterface;

/**
 * Class BaseReq
 * @property ReqHead $reqHead
 * @package by\component\tailong_bank\req
 */
abstract class BaseReq implements ObjectToArrayInterface
{
    protected $reqHead;
    protected $seqNo;

    public function toArray()
    {
        return Obj2ArrayExtendHelper::getArrayFrom($this);
    }

    /**
     * @return mixed
     */
    public function getReqHead()
    {
        return $this->reqHead;
    }

    /**
     * @param mixed $reqHead
     */
    public function setReqHead($reqHead)
    {
        $this->reqHead = $reqHead;
    }

    /**
     * @return mixed
     */
    public function getSeqNo()
    {
        return $this->seqNo;
    }

    /**
     * @param mixed $seqNo
     */
    public function setSeqNo($seqNo)
    {
        $this->seqNo = $seqNo;
    }
}