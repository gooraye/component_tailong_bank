<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-12 17:52
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\req;


class ApproveDevReq
{
    private $random;
    private $seqNo;

    // construct
    public function __construct()
    {
        // TODO construct
    }

    /**
     * @return mixed
     */
    public function getRandom()
    {
        return $this->random;
    }

    /**
     * @param mixed $random
     */
    public function setRandom($random)
    {
        $this->random = $random;
    }

    /**
     * @return mixed
     */
    public function getSeqNo()
    {
        return $this->seqNo;
    }

    /**
     * @param mixed $seqNo
     */
    public function setSeqNo($seqNo)
    {
        $this->seqNo = $seqNo;
    }
}