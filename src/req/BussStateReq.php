<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-17 15:33
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\req;


class BussStateReq extends BaseReq
{
    private $orderId;

    public function toArray()
    {
        $stdCls = new \stdClass();
        $stdCls->orderId = $this->getOrderId();
        return [
            'head' => $this->reqHead->toArray(),
            'body' => [
                'orderId' => $this->getOrderId()
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }
}