<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-26 15:45
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\tailong_bank\req;


use by\infrastructure\helper\Object2DataArrayHelper;

class PushSysNoticeReq extends BaseReq
{
    private $sysNoticeCode;
    private $sysNoticeCntnt;
    private $original;
    private $productId;
    private $bussSeqNo;

    public function __construct($data = [])
    {
        $this->reqHead = new ReqHead();
        if (array_key_exists('_reqData', $data)) {
            $reqData = $data['_reqData'];
            if (array_key_exists('head', $reqData)) {
                Object2DataArrayHelper::setData($this->reqHead, $reqData['head']);
            }
        }
        unset($data['_reqData']);
        Object2DataArrayHelper::setData($this, $data);
    }

    /**
     * @return mixed
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * @param mixed $original
     */
    public function setOriginal($original)
    {
        $this->original = $original;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getBussSeqNo()
    {
        return $this->bussSeqNo;
    }

    /**
     * @param mixed $bussSeqNo
     */
    public function setBussSeqNo($bussSeqNo)
    {
        $this->bussSeqNo = $bussSeqNo;
    }

    /**
     * @return mixed
     */
    public function getSysNoticeCode()
    {
        return $this->sysNoticeCode;
    }

    /**
     * @param mixed $sysNoticeCode
     */
    public function setSysNoticeCode($sysNoticeCode)
    {
        $this->sysNoticeCode = $sysNoticeCode;
    }

    /**
     * @return mixed
     */
    public function getSysNoticeCntnt()
    {
        return $this->sysNoticeCntnt;
    }

    /**
     * @param mixed $sysNoticeCntnt
     */
    public function setSysNoticeCntnt($sysNoticeCntnt)
    {
        $this->sysNoticeCntnt = $sysNoticeCntnt;
    }
}